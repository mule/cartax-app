﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoVeroPdfParser
{
    public class CarMakeDictionaryCreator
    {
        public static IEnumerable<string> CreateMakeDictionary( IEnumerable<string[]> datalines)
        {

            var possibleMakes = datalines.Select(line => line[0].ToLower()).Distinct();



            return possibleMakes.ToArray();
        }
    }
}
