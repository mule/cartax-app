﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoVeroPdfParser.Data;

namespace AutoVeroPdfParser
{
    public class DataLineSearchResult
    {
        public string[] Fields { get; set; }
        public ImportedCar ParsedData { get; set; }
        public List<Tuple< CarTaxPdfParser.ImportedCarFields, int >> FoundFieldIndexes { get; set; }
        public int MakeFieldEndIndex { get; set; }

        public DataLineSearchResult()
        {

            FoundFieldIndexes = new List<Tuple<CarTaxPdfParser.ImportedCarFields, int>>();
            ParsedData = new ImportedCar();
            
        }


        //TODO: add to string

    }
}
