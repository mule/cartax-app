﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoVeroPdfParser.Data;
using Lucene.Net.Analysis;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Version = Lucene.Net.Util.Version;

namespace AutoVeroPdfParser
{
    public class CarTaxPdfParser 
    {

        

        public enum ImportedCarFields
        {
            KäyttöönottoPvm,
            PäätösPvm,
            Kunto,
            Unknown = 0
        };

        public IEnumerable<string> CarMakes { get; set; }

        public IEnumerable<DataLineSearchResult> SearchPdfForDataLines( Stream stream)
        {
            var reader = new PdfReader(stream);
            var pageCount = reader.NumberOfPages;
            var lines = new List<string>();

            var foundDataLines = new List<DataLineSearchResult>();
            for (int page = 2; page <= pageCount; page++)
            {
                var pageStr = PdfTextExtractor.GetTextFromPage(reader, page);
                lines.AddRange(pageStr.Split(new char[] {'\n'}, StringSplitOptions.RemoveEmptyEntries).ToList());
                
            }

            foreach (var line in lines)
            {
                var searchResult = new DataLineSearchResult();

                if (detectDataLine(line, out searchResult))
                {
                    

                    foundDataLines.Add(searchResult);
                }

            }


            return foundDataLines;
        }

        public  ImportedCar ParseDataLineText(DataLineSearchResult searchResult, IndexSearcher searcher)
        {
            //TODO: Refactor this. Does not really need to be in a private method
            return parseRemainingAutoveroFields(searchResult, searcher);

        }




        private  bool detectDataLine(string data, out DataLineSearchResult dataLine)
        {
            dataLine = new DataLineSearchResult();
            var fields = data.Split(' ');

            bool accepted = false;

            var date1Found = false;
            var date2Found = false;
            var kuntoFound = false;

            for (int fieldIndx = fields.Length - 1; fieldIndx >= 0; fieldIndx--)
            {
                var fieldStr = fields[fieldIndx];
                var result = new DateTime();

            

                if (fieldIndx < 7)
                    break;

               if (DateTime.TryParseExact(fieldStr, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal,
                                          out
                                              result))
               {
                   date1Found = true;
                   dataLine.FoundFieldIndexes.Add( new Tuple<ImportedCarFields, int>( ImportedCarFields.KäyttöönottoPvm,fieldIndx));
                   dataLine.ParsedData.KäyttöönottoPvm = result;
                 
                   fieldStr = fields[fieldIndx - 1];


                   date2Found = DateTime.TryParseExact(fieldStr, "yyyyMMdd", CultureInfo.CurrentCulture,
                                                       DateTimeStyles.AssumeLocal,
                                                       out
                                                           result);

                   if (date2Found)
                   {
                       dataLine.FoundFieldIndexes.Add(new Tuple<ImportedCarFields, int>( ImportedCarFields.PäätösPvm, fieldIndx-1));
                       dataLine.ParsedData.PäätösPvm = result;
                       fieldStr = fields[fieldIndx - 2];
                       kuntoFound = detectKunto(fieldStr);
                       if(kuntoFound)
                       {
                           dataLine.FoundFieldIndexes.Add( new Tuple<ImportedCarFields, int>( ImportedCarFields.Kunto,fieldIndx-2));
                           dataLine.ParsedData.Kunto = fieldStr;

                       }


                   }
                   dataLine.Fields = fields;
                   return date2Found && kuntoFound;

               }
               
            }

            return false;
        }


        private ImportedCar parseRemainingAutoveroFields(DataLineSearchResult result, IndexSearcher searcher)
        {
            double ajoKm;
            double verotusarvo;
            double autovero;
            var firstOrDefault = result.FoundFieldIndexes.FirstOrDefault(fld => fld.Item1 == ImportedCarFields.KäyttöönottoPvm);
            if (firstOrDefault != null)
            {
                var käyttöönottoPvmIndx =
                    firstOrDefault.Item2;
                int ajoKmIndx;
                int verotusarvoIndx;
                int autoveroIndx; 
                bool hasAjoKmField = result.Fields.Length - käyttöönottoPvmIndx == 4;

                if(hasAjoKmField && result.Fields.Length > käyttöönottoPvmIndx+3)
                {
                    ajoKmIndx = käyttöönottoPvmIndx + 1;
                    verotusarvoIndx = käyttöönottoPvmIndx + 2;
                    autoveroIndx = käyttöönottoPvmIndx + 3;

                    if(!Double.TryParse(result.Fields[ajoKmIndx], NumberStyles.Number,CultureInfo.InvariantCulture, out ajoKm))
                        Trace.TraceError(String.Format("Failed to parse ajoKm: {0} ", result.Fields[käyttöönottoPvmIndx + 1]));
                    else
                        result.ParsedData.AjoKm = ajoKm;
                }
                else
                {
                    verotusarvoIndx = käyttöönottoPvmIndx + 1;
                    autoveroIndx = käyttöönottoPvmIndx + 2;
                }

                if (!Double.TryParse(result.Fields[verotusarvoIndx], NumberStyles.Number, CultureInfo.InvariantCulture, out verotusarvo))
                    Trace.TraceError(String.Format("Failed to parse verotusarvo: {0} ", result.Fields[verotusarvoIndx]));
                else
                    result.ParsedData.Verotusarvo = verotusarvo;


                if (!Double.TryParse(result.Fields[autoveroIndx], NumberStyles.Number, CultureInfo.InvariantCulture, out autovero))
                    Trace.TraceError(String.Format("Failed to parse autovero: {0} ", result.Fields[autoveroIndx]));
                else
                    result.ParsedData.Autovero = autovero;
            }
            else
            {
                //We should not be here ever

                Trace.TraceError("Invalid search result passed to parseRemainingAutoveroFields");


            }

            parseCarMake(ref result,  searcher);

            return result.ParsedData;


        }


        private bool parseVerotusarvo(string verotusArvoStr, out double result)
        {
           
          
            var parseSuccess = false;

            result = new double();
            if (!String.IsNullOrEmpty(verotusArvoStr))
            {
                parseSuccess = Double.TryParse(verotusArvoStr, out result);
            }


            return parseSuccess;
        }

        private bool parseAjoKm(string[] fields, out double result)
        {
            var autoveroStr = fields[fields.Length - 3];
            var parseSuccess = false;

            result = new double();
            if (!String.IsNullOrEmpty(autoveroStr))
            {
                parseSuccess = Double.TryParse(autoveroStr, out result);
            }

            result = result*1000;
            return parseSuccess;
        }

        private bool parseKäyttöönottoPvm(string[] fields, out DateTime result)
        {
            var autoveroStr = fields[fields.Length - 4];
            var parseSuccess = false;

            result = new DateTime();
            if (!String.IsNullOrEmpty(autoveroStr))
            {
                parseSuccess = DateTime.TryParseExact(autoveroStr,"yyyyMMdd", CultureInfo.GetCultureInfo("fi-FI"),DateTimeStyles.None, out result);
            }

            
            return parseSuccess;
        }

        private bool parsePäätösPvm(string[] fields, out DateTime result)
        {
            var autoveroStr = fields[fields.Length - 5];
            var parseSuccess = false;

            result = new DateTime();
            if (!String.IsNullOrEmpty(autoveroStr))
            {
                parseSuccess = DateTime.TryParseExact(autoveroStr, "yyyyMMdd", CultureInfo.GetCultureInfo("fi-FI"), DateTimeStyles.None, out result);
            }


            return parseSuccess;
        }

        private bool detectKunto(string data)
        {
            
            var parseSuccess = false;

    
            if (!String.IsNullOrEmpty(data))
            {
                var trimmed = data.Trim();
                if (trimmed == "H" || trimmed == "N" || trimmed == "Y")
                {
                    parseSuccess = true;
                    

                }
                    
            }

           
            return parseSuccess;
        }

        private void parseCarMake(ref DataLineSearchResult searchResult, IndexSearcher searcher )
        {
            if(searchResult.Fields.Length>2)
            {
                var possibleMakeText = String.Format("{0} {1}", searchResult.Fields[0].ToLower(),
                                                     searchResult.Fields[1].ToLower());

                var term1 = new Term("makename", searchResult.Fields[0].ToLower());

                

                var query1 = new FuzzyQuery(term1,0.3f);
              

                var term2 = new Term("makename", searchResult.Fields[1].ToLower());

                var query2 = new TermQuery(term2);



                var query3 = new BooleanQuery();
                query3.Add( new BooleanClause(query1,BooleanClause.Occur.MUST));
                query3.Add(new BooleanClause(query2, BooleanClause.Occur.SHOULD));
                
            


                var docs = searcher.Search(query1, 1);
                var topResult = docs.ScoreDocs.FirstOrDefault();

            
     
                if(topResult==null)
                    Trace.WriteLine(String.Format( "Unknown make: {0}", possibleMakeText));
                else
                {
                    var make = searcher.Doc(topResult.doc).GetField("makename").StringValue();
                    searchResult.ParsedData.Merkki = make;
                    searchResult.MakeFieldEndIndex = make.Length;
                }

            }
        }

    }
}
