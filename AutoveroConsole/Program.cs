﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Autovero;
using AutoveroDAL.Data;

namespace AutoveroConsole
{
    internal class Program
    {
        public static BlobStorageFileUploader blobUploader = new BlobStorageFileUploader();
        public static List<BlobStoreFile> newFiles = new List<BlobStoreFile>();  

        private static void Main(string[] args)
        {



            blobUploader.FileExists += blobUploader_FileExists;
            blobUploader.FileUploaded += blobUploader_FileUploaded;



            DownloadPdfs().Wait();




        }

        private static void blobUploader_FileUploaded(object sender, EventArgs e)
        {
            var args = e as FileEventArgs;
            if (args != null) Trace.WriteLine(String.Format("File {0} uploaded.", args.Filename));
        }

        private static void blobUploader_FileExists(object sender, EventArgs e)
        {
            var args = e as FileEventArgs;
            if (args != null) Trace.WriteLine(String.Format("File {0} already exists", args.Filename));
        }


        public static async Task DownloadPdfs()
        {
            var rootUrl = "http://www.tulli.fi/fi/yksityisille/autoverotus/taulukot/autot/index.jsp";
            var client = new HttpClient();

            var rootStream = await client.GetStreamAsync(rootUrl);
            var fileUrls = PdfUrlFinder.FetchPdfUrls(rootStream);

            var transfers = new List<Task>();


            foreach (string fileUrl in fileUrls)
            {
                var fileName = fileUrl.Split('/').Last();

                var memStream = new MemoryStream();

                transfers.Add(FileFetcher.FetchFileStreamFromUrl(fileUrl, memStream).ContinueWith(dt =>
                                                                                                      {
                                                                                                          var downloadStream=dt.Result;

                                                                                                          downloadStream.Position= 0;
                                                                                                          blobUploader.UploadFile(fileName,downloadStream)
                                                                                                              .ContinueWith((ut) =>downloadStream.Close());
                                                                                                      }));

            }

            Task.WaitAll(transfers.ToArray());
        }
    }
}

