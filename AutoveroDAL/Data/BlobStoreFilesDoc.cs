﻿using System;
using System.Collections.Generic;

using System.Linq;


namespace AutoveroDAL.Data
{

    public class BlobStoreFilesDoc
    {
        

        public BlobStoreFile[] Files;

        public BlobStoreFilesDoc()
        {
            Files = new BlobStoreFile[0];
        }


    }

    public class BlobStoreFile
    {
        public Guid Id { get; set; }
        public  string Filename { get; set; }
        public string Url { get; set; }
        public bool Parsed { get; set; }

        public override string ToString()
        {
            return String.Format("BlobStoreFile data: Id {3} Filename {0} Url {1} Parsed {2}", Filename, Url, Parsed, Id);
        }

    }


}