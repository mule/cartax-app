﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoVeroPdfParser.Data
{
    public class ImportedCar
    {
 


        public double Autovero { get; set; }
        public double Verotusarvo { get; set; }
        public double AjoKm { get; set; }
        public DateTime KäyttöönottoPvm { get; set; }
        public DateTime PäätösPvm { get; set; }
        public string  Kunto { get; set; }
        public string MalliTarkemmin { get; set; }
        public string Malli { get; set; }
        public string Merkki { get; set; }

        public override string ToString()
        {
            var bldr = new StringBuilder();
            bldr.AppendFormat("Merkki: {0}", Merkki);
            bldr.AppendFormat("Autovero: {0} ", Autovero.ToString(CultureInfo.CurrentCulture));
            bldr.AppendFormat("Verotusarvo: {0} ", Verotusarvo.ToString(CultureInfo.CurrentCulture));
            bldr.AppendFormat("AjoKm: {0} ", AjoKm.ToString(CultureInfo.CurrentCulture));
            bldr.AppendFormat("KäyttöönottoPvm: {0} ", KäyttöönottoPvm.ToShortDateString());
            bldr.AppendFormat("Päätöspvm: {0} ", PäätösPvm.ToShortDateString());
            bldr.AppendFormat("Kunto: {0}", Kunto);

            return bldr.ToString();

        }


    }
}
