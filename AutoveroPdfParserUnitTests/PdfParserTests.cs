﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using AutoVeroPdfParser;
using AutoVeroPdfParser.Data;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Newtonsoft.Json;
using Xunit;

namespace AutoveroPdfParserUnitTests
{
    public class PdfParserTests
    {


        [Fact]
        public void Should_find_datalines_without_exception()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(PdfParserTests));
            var searchResults = new List<DataLineSearchResult>();

            var parser = new CarTaxPdfParser();
            using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.1209.pdf"))
            {
                searchResults = parser.SearchPdfForDataLines(stream).ToList();


            }


            foreach (var result in searchResults)
            {
                Trace.WriteLine(result);
            }
        }


        [Fact]
        public void Should_get_car_makes_from_data_text()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(PdfParserTests));
            var searchResults = new List<DataLineSearchResult>();

            var parser = new CarTaxPdfParser();
            using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.1209.pdf"))
            {
                searchResults = parser.SearchPdfForDataLines(stream).ToList();


            }


            var fields = searchResults.Select(rslt => rslt.Fields);

            var makes = CarMakeDictionaryCreator.CreateMakeDictionary(fields);
            var makesJson = JsonConvert.SerializeObject(makes, Formatting.Indented);
            Trace.WriteLine(makesJson);

            foreach (string make in makes)
            {


                Trace.WriteLine(make);
            }



        }




        [Fact]
        public void Should_parse_car_makes_correctly()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(PdfParserTests));
            var searchResults = new List<DataLineSearchResult>();
            var makeReferences = new List<String>();
            var importedCars = new List<ImportedCar>();


            using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.carMakes.json"))
            {
                using (var reader = new StreamReader(stream))
                {
                    var jsonStr = reader.ReadToEnd();

                    makeReferences = JsonConvert.DeserializeObject<List<string>>(jsonStr);

                }

            }

            using (var directory = new RAMDirectory())
            {
                var analyzer = new StandardAnalyzer();
                
                    using (var writer = new IndexWriter(directory, analyzer,true, IndexWriter.MaxFieldLength.UNLIMITED))
                    {

                        foreach (string makeReference in makeReferences)
                        {
                            var doc = new Document();
                            doc.Add(new Field("makename", makeReference, Field.Store.YES, Field.Index.ANALYZED));

                            writer.AddDocument(doc);
                        }

                        writer.Commit();
                        writer.WaitForMerges();

                    }
                

                using (var indxReader = IndexReader.Open(directory, true))
                {
                    {
                        for (int i = 0; i < indxReader.MaxDoc(); i++)
                        {
                            if (indxReader.IsDeleted(i)) continue;

                            Document d = indxReader.Document(i);
                            var fields = d.GetFields();

                            foreach (var fld in fields)
                            {
                                Trace.WriteLine(fld);
                            }
                        }
                    }
                }

                var parser = new CarTaxPdfParser();

                using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.1209.pdf"))
                {
                    searchResults = parser.SearchPdfForDataLines(stream).ToList();

                }

                var searcher = new IndexSearcher(directory);

                foreach (DataLineSearchResult dataLineSearchResult in searchResults)
                {
                    var carData = parser.ParseDataLineText(dataLineSearchResult, searcher);

                    Trace.WriteLine(carData);
                    importedCars.Add(carData);

                }
            }








        }

        //[Fact]
        //public void Should_parse_imported_cars_data_correctly()
        //{

        //    Assembly assembly = Assembly.GetAssembly(typeof(PdfParserTests));
        //    List<string> lines = new List<string>();

        //    var parser = new CarTaxPdfParser();

        //    using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.1209.pdf"))
        //    {
        //        lines = parser.SearchPdfForDataLines(stream).ToList();


        //    }

        //    List<ImportedCar> parsedData = new List<ImportedCar>();


        //    foreach (string line in lines)
        //    {
        //        var car = parser.ParseDataLineText(line);

        //        Trace.WriteLine(car);
        //    }

        //}

    }
}
