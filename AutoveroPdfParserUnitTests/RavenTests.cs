﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoveroDAL.Data;
using Raven.Client.Document;
using Raven.Client.Embedded;
using Xunit;
using Xunit.Extensions;

namespace AutoveroPdfParserUnitTests
{
  public  class RavenTests
  {
      private DocumentStore ravenDb;


      public RavenTests()
      {
          ravenDb = new EmbeddableDocumentStore {RunInMemory = true};
          ravenDb.Initialize();
      }



      [Fact]
      public void Should_create_file_list_document_to_raven()
      {

          var actual = new BlobStoreFilesDoc();
          var fileData = new BlobStoreFile() {Filename = "test", Parsed = false, Url = "testurl"};

          actual.Files = new []{fileData};



          
        using (var session = ravenDb.OpenSession())
        {
           
            session.Store(actual);
            session.SaveChanges();
            
        }

          using (var session = ravenDb.OpenSession())
          {
              var results = (from filesDoc in session.Query<BlobStoreFilesDoc>()
                             select filesDoc).ToArray();


              foreach (BlobStoreFilesDoc blobStoreFilesDoc in results)
              {
                  Trace.WriteLine(blobStoreFilesDoc.Files.First());


              }

      

          }
         

      }


  }
}
