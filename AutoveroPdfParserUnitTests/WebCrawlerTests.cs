﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoVeroPdfParser;
using AutoVeroPdfParser.Data;
using Autovero;
using Newtonsoft.Json;
using Xunit;
using System.Net.Http;

namespace AutoveroPdfParserUnitTests
{
    public class WebCrawlerTests
    {
        [Fact]
// ReSharper disable InconsistentNaming
        public void Should_find_cartax_pdf_links_from_rootPage()
// ReSharper restore InconsistentNaming
        {
            Assembly assembly = Assembly.GetAssembly(typeof(PdfParserTests));


            var links = new List<string>();

            using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.RootPage.html"))
            {

                links = PdfUrlFinder.FetchPdfUrls(stream).ToList();


            }


            foreach (string link in links)
            {
                Trace.WriteLine(link);
            }
            
            Assert.True(links.Count==28); 
        }


        [Fact]
        public async Task Should_fetch_files_from_given_uris()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(PdfParserTests));

            
            var links = new List<string>();

            using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.RootPage.html"))
            {

                links = PdfUrlFinder.FetchPdfUrls(stream).ToList();

            }


            using (var fileStream = new FileStream("Test.pdf", FileMode.Create,FileAccess.Write,FileShare.None))
            {
                await FileFetcher.FetchFileStreamFromUrl(links.FirstOrDefault(), fileStream).ContinueWith((copyTask) =>
                                                                                                              {
                                                                                                                  fileStream.Close();
                                                                                                              });

            }

         



        }

        [Fact]
        public async Task Should_upload_files_to_blob_storage()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(WebCrawlerTests));
            var uploader = new BlobStorageFileUploader();

            uploader.FileExists += (sender, args) => { Trace.WriteLine("File already exists"); };
            uploader.FileUploaded += (sender, args) => { Trace.WriteLine("File uploaded"); };
                

            using (var stream = assembly.GetManifestResourceStream("AutoveroPdfParserUnitTests.TestFiles.1209.pdf"))
            {
                await uploader.UploadFile("Test.pdf", stream);

            }




           




        }



        
    }
}
