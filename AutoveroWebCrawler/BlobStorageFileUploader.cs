﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Autovero
{
    public class BlobStorageFileUploader
    {

        public static string DefaultContainerName = "tullipdffiles";

        public event EventHandler FileExists;
        public event EventHandler FileUploaded;

        public BlobStorageFileUploader()
        {



        }


        private CloudBlobClient UseConfigurationFile() { 
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["BlobStorageConnectionString"]); 
            CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

            return cloudBlobClient;
        }



        public async Task<bool> UploadFile(string filename, Stream fileStream )
        {
            var client = UseConfigurationFile();


            var container = CheckContainer(client);

            var blob = container.GetBlockBlobReference(filename);


            var taskCheck =
                Task.Factory.FromAsync<bool>(blob.BeginExists, blob.EndExists, null);

           var  fileExists =  await taskCheck;

            if (fileExists)
            {
              
                OnFileExists(new FileEventArgs() { Filename = filename });
                return false;
            }
            else
            {
                var uploadTask = Task.Factory.FromAsync(blob.BeginUploadFromStream, blob.EndUploadFromStream, fileStream, null);

               await uploadTask;
               OnFileUploaded(new FileEventArgs(){Filename = filename, BlobUrl = blob.Uri.ToString()});
                return true;


            }
     
        }



        private CloudBlobContainer CheckContainer(CloudBlobClient client)
        {
            var pdfContainer = client.GetContainerReference(DefaultContainerName);
            pdfContainer.CreateIfNotExists();


     
            return pdfContainer;
        }








        protected virtual void OnFileExists(FileEventArgs  e)
        {

            var handler = FileExists;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnFileUploaded(FileEventArgs e)
        {
            var handler = FileUploaded;

            if (handler != null)
            {
                handler(this, e);
            }
            
        }






    }

    public class FileEventArgs : EventArgs
    {
        public string Filename { get; set; }
        public string  BlobUrl { get; set; }
    }
}
