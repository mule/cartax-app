﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Autovero
{
    public class FileFetcher
    {


        public static async Task<Stream> FetchFileStreamFromUrl(string fileUrl, Stream targetStream)
        {
            var tasks = new List<Task<Stream>>();
            var client = new HttpClient();


            var response = await client.GetAsync(fileUrl);

             await response.Content.CopyToAsync(targetStream);

            return targetStream;
        }


    }
}
