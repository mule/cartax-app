﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Autovero
{
    public class PdfUrlFinder
    {
        
        [Pure]
        public static IEnumerable<string> FetchPdfUrls(Stream rootPageStream)
        {
             Contract.Requires(rootPageStream !=null);
            
            var rootDoc = new HtmlDocument();
            rootDoc.Load(rootPageStream,true);

            var result = new  List<string>();

            var pdfLinks = rootDoc.DocumentNode.SelectNodes(
                "//a[@href]");


            if (pdfLinks != null)
            {
                foreach (HtmlNode htmlNode in pdfLinks)
                {
                    var link = htmlNode.Attributes["href"].Value;
                    if(link.EndsWith("pdf"))
                         result.Add(link);
                }
                 
            }

            return result;
        }
    }
}
